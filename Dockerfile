FROM alpine:3 as build
RUN mkdir /app
RUN mkdir /publish
COPY ./app /app/
RUN apk add --no-cache dotnet6-sdk libstdc++
CMD dotnet publish -l:diag -r linux-musl-x64 --self-contained -o /publish /app/BlazorServerDbContextExample.csproj

FROM alpine:3
RUN mkdir /publish
COPY --from=build /publish /publish
RUN apk add --no-cache libstdc++ icu-data-full icu-libs
EXPOSE 5000
ENV ASPNETCORE_URLS=http://+:5000
ENV ASPNETCORE_ENVIRONMENT=Development
CMD cd /publish && ./BlazorServerDbContextExample